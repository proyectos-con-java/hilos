package uso_threads;

import java.awt.geom.*;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class UsoThreads {
	public static void main(String[] args) {
		JFrame marco=new MarcoRebote();
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		marco.setVisible(true);
	}
}

class PelotaHilos implements Runnable{

	private Pelota pelota;
	private Component componente;
	
	public PelotaHilos(Pelota p, Component c){
		pelota = p;
		componente = c;
	}
	
	@Override
	public void run() {
		while(!Thread.currentThread().interrupted()){			
			pelota.mueve_pelota(componente.getBounds());		
			componente.paint(componente.getGraphics());	
			try {
				Thread.sleep(4);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}
}

//Movimiento de la pelota-----------------------------------------------------------------------------------------
class Pelota{	
	private static final int TAMX=15;	
	private static final int TAMY=15;
	private double x=0;
	private double y=0;	
	private double dx=1;	
	private double dy=1;
	
	// Mueve la pelota invirtiendo posici�n si choca con l�mites	
	public void mueve_pelota(Rectangle2D limites){	
		x+=dx;		
		y+=dy;
		
		if(x<limites.getMinX()){			
			x=limites.getMinX();			
			dx=-dx;
		}
		
		if(x + TAMX>=limites.getMaxX()){			
			x=limites.getMaxX() - TAMX;			
			dx=-dx;
		}
		
		if(y<limites.getMinY()){			
			y=limites.getMinY();			
			dy=-dy;
		}
		
		if(y + TAMY>=limites.getMaxY()){
			y=limites.getMaxY()-TAMY;			
			dy=-dy;			
		}		
	}
	
	//Forma de la pelota en su posici�n inicial	
	public Ellipse2D getShape(){		
		return new Ellipse2D.Double(x,y,TAMX,TAMY);		
	}		
}

// L�mina que dibuja las pelotas----------------------------------------------------------------------
class LaminaPelota extends JPanel{
	private ArrayList<Pelota> pelotas=new ArrayList<Pelota>();
	
	//A�adimos pelota a la l�mina	
	public void add(Pelota b){		
		pelotas.add(b);
	}
	
	public void paintComponent(Graphics g){		
		super.paintComponent(g);		
		Graphics2D g2=(Graphics2D)g;		
		for(Pelota b: pelotas){			
			g2.fill(b.getShape());
		}		
	}	
}

//Marco con l�mina y botones------------------------------------------------------------------------------
class MarcoRebote extends JFrame{
	private LaminaPelota lamina;
	Thread t,t1,t2,t3;
	JButton btn1,btn2,btn3;
	JButton det1,det2,det3;
	
	public MarcoRebote(){	
		setBounds(600,300,600,350);	
		setTitle ("Rebotes");		
		lamina=new LaminaPelota();		
		add(lamina, BorderLayout.CENTER);		
		JPanel laminaBotones=new JPanel();	
			
		btn1 = new JButton("Hilo 1");
		btn1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		}); 
		laminaBotones.add(btn1);
		
		btn2 = new JButton("Hilo 2");
		btn2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		}); 
		laminaBotones.add(btn2);
		
		btn3 = new JButton("Hilo 3");
		btn3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				comienza_el_juego(e);
			}
		}); 
		laminaBotones.add(btn3);
		
		det1 = new JButton("Detener 1");
		det1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		}); 
		laminaBotones.add(det1);
		
		det2 = new JButton("Detener 2");
		det2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		}); 
		laminaBotones.add(det2);
		
		det3 = new JButton("Detener 3");
		det3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				detener(e);
			}
		}); 
		laminaBotones.add(det3);
		
		add(laminaBotones, BorderLayout.SOUTH);
	}
		
	//Ponemos botones
	public void ponerBoton(Container c, String titulo, ActionListener oyente){		
		JButton boton=new JButton(titulo);	
		c.add(boton);	
		boton.addActionListener(oyente);	
	}
	
	//A�ade pelota y la bota 1000 veces
	public void comienza_el_juego (ActionEvent e){				
		Pelota pelota=new Pelota();		
		lamina.add(pelota);			
		
		Runnable r = new PelotaHilos(pelota, lamina);
		
		if(e.getSource().equals(btn1)){
			t1 = new Thread(r);	
			t1.start();
		}else if(e.getSource().equals(btn2)){
			t2 = new Thread(r);	
			t2.start();
		}else if(e.getSource().equals(btn3)){
			t3 = new Thread(r);	
			t3.start();
		}
	}	
	
	public void detener(ActionEvent e){
		if(e.getSource().equals(det1)){
			t1.interrupt();
		}else if(e.getSource().equals(det2)){
			t2.interrupt();
		}else if(e.getSource().equals(det3)){
			t3.interrupt();
		}
	}
}